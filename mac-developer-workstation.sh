#!/bin/bash
# ---------------------------------------------------------------------------- #
#
#              _______      __________             .__
#              \      \     \______   \____ _______|__| ______
#              /   |   \     |     ___|__  \\_  __ \  |/  ___/
#             /    |    \    |    |    / __ \|  | \/  |\___ \
#             \____|__  / /\ |____|   (____  /__|  |__/____  >
#                     \/  \/               \/              \/
#
# ---------------------------------------------------------------------------- #
#
# Installs everything I need to develop on a Mac workstation
# - Checks the presence of CURL
# - Install: Git, Oh-My-Zsh, Oh My Zsh powerline theme, Composer, Symfony, etc.
#

AUTHOR="Nicolas PARIS <nikoquattro@gmail.com>"

# Définition des variables
# -----------------------
RED='\x1B[1;31m'
GREEN='\x1B[1;32m'
BLUE='\x1B[1;34m'
DEFAULT='\x1B[0;m'

NOW=$(date +"%m-%d-%Y")

# Fonctions
# ---------
function cmd_print {
    cmdLine="$1"
    cmdStringProcess=`printf "%-45s" "$2"`
    cmdString=`printf "%-50s" "$2"`

    echo -ne "*** $cmdStringProcess${BLUE}[PROCESS]${DEFAULT} ***"
    eval $cmdLine $redirect
    if [ $? -eq 0 ]
    then
        echo -e "\r*** $cmdString${GREEN}[OK]${DEFAULT} ***"
    else
        echo -e "\r*** $cmdString${RED}[KO]${DEFAULT} ***"
    fi
}

# Vérification que le script est bien executé en tant que 'root'
# --------------------------------------------------------------
if [[ $EUID -ne 0 ]]; then
    clear
    echo "******************************************************************************"
    echo "***                                                                        ***"
    echo "***                                                                        ***"
    echo -e "***                 ${RED}!!! This script must be run as root !!!${DEFAULT}                ***"
    echo "***                                                                        ***"
    echo "***                                                                        ***"
    echo -e "*** ${RED}You must run the script with the command 'sudo'${DEFAULT}                        ***"
    echo -e "*** ${RED}Make sure you have read and understood this script before executing it${DEFAULT} ***"
    echo "******************************************************************************"
    1>&2
    exit 1
fi

# Création d'un fichier de log
# ----------------------------
logFile="/Library/Logs/mac-dev-workstation-$NOW.log"
sudo touch $logFile
echo "$(date "+%Y.%m.%d %H:%M:%S")" >> $logFile 2>&1

# Vérification que CURL est bien installé
# ---------------------------------------
if ! which curl >/dev/null; then
    echo "******************************************************************************"
    echo -e "***                       ${RED}!!! CURL is not install !!!${DEFAULT}                      ***"
    echo "***                                                                        ***"
    echo -e "*** ${RED}You must install CURL in order to continue${DEFAULT}                             ***"
    echo "******************************************************************************"
fi

# Vérification que GIT est bien installé
# ---------------------------------------
if which git >/dev/null; then
    echo -e "***                       ${GREEN}!!! GIT is install !!!${DEFAULT}                           ***"
    echo -e "*** ${BLUE}$(git --version)${DEFAULT}                                      ***"
else
    echo "******************************************************************************"
    echo -e "***                       ${RED}!!! GIT is not install !!!${DEFAULT}                      ***"
    echo "***                                                                        ***"
    echo -e "*** ${RED}You must install CURL in order to continue${DEFAULT}                             ***"
    echo "******************************************************************************"
fi

# Install Oh-My-Zsh framework
# ---------------------------
if sh -c "$(curl --silent https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"; then
    echo "******************************************************************************"
    echo -e "*** ${GREEN}Oh-My-Zsh framework is correctly installed${DEFAULT}                             ***"
    echo "******************************************************************************"
fi
